#!/usr/bin/env python3

import pymysql
import csv

# Open database connection
db = pymysql.connect("mariadbtitanic","root","apiexercice","titanic" )

# Import Data into the Database
cursor = db.cursor()

# Create table as per requirement
sql0 = """CREATE TABLE IF NOT EXISTS titanicdata(
   uuid varchar(36) NOT NULL,
   survived  boolean NOT NULL,
   passengerClass  INT,
   name CHAR(100),
   sex CHAR(20),
   age INT,
   siblingsOrSpousesAboard INT,
   parentsOrChildrenAboard INT,
   fare FLOAT)"""

cursor.execute(sql0)

# Create trigger to generate uuid
sql1 = """CREATE TRIGGER IF NOT EXISTS before_insert_mytable
  BEFORE INSERT ON titanicdata
  FOR EACH ROW
  SET new.uuid = uuid();"""

cursor.execute(sql1)


# Import data into mysql
titanic_data = csv.reader(open('titanic.csv'))
for row in titanic_data:
        cursor.execute('INSERT INTO titanicdata (uuid,survived, passengerClass, name, sex, age, siblingsOrSpousesAboard, parentsOrChildrenAboard, fare )' 'VALUES(uuid(),%s,%s,%s,%s,%s,%s,%s,%s)', row)

db.commit()
cursor.close()
print ("Done")

# Disconnect from server
db.close()
