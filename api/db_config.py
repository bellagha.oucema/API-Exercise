from app import app
from flaskext.mysql import MySQL

mysql = MySQL()

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'apiexercice'
app.config['MYSQL_DATABASE_DB'] = 'titanic'
app.config['MYSQL_DATABASE_HOST'] = 'mariadbtitanic'
mysql.init_app(app)
