import pymysql
from app import app
from db_config import mysql
from flask import flash, request, render_template, redirect, jsonify


#GET /people
@app.route('/people')
def get_peoples():
	try:
		conn = mysql.connect()
		cursor = conn.cursor(pymysql.cursors.DictCursor)
		cursor.execute("SELECT * FROM titanicdata")
		rows = cursor.fetchall()
		resp = jsonify(rows)
		resp.status_code = 200
		return resp
	except Exception as e:
		print(e)
	finally:
		cursor.close()
		conn.close()

#POST /people

#FIXME
@app.route('/people', methods=['POST'])
def add_post_people():
	try:
		_json = request.json
		_survived		 		 = _json['survived']
		_passengerClass  		 = _json['passengerClass']
		_name 			 		 = _json['name']
		_sex 			 		 = _json['sex']
		_age 					 = _json['age']
		_siblingsOrSpousesAboard = _json['siblingsOrSpousesAboard']
		_parentsOrChildrenAboard = _json['parentsOrChildrenAboard']
		_fare 					 = _json['fare']
		# validate the received values
		if _sex and _name and request.method == 'POST':

			# save edits
			sql = "INSERT INTO titanicdata (survived, passengerClass, name, sex, age, siblingsOrSpousesAboard, parentsOrChildrenAboard, fare ) VALUES(%s,%s,%s,%s,%s,%s,%s,%s)"
			data = (_survived, _passengerClass, _name, _sex, _age, _siblingsOrSpousesAboard, _parentsOrChildrenAboard, _fare, )
			conn = mysql.connect()
			cursor = conn.cursor()
			cursor.execute(sql, data)
			conn.commit()
			resp = jsonify('People added successfully!')
			resp.status_code = 200
			return resp
		else:
			return not_found()
	except Exception as e:
		print(e)
	finally:
		cursor.close()
		conn.close()

#GET /people/{uuid}

@app.route('/people/<id>')
def get_one_people(id):
	try:
		conn = mysql.connect()
		cursor = conn.cursor(pymysql.cursors.DictCursor)
		cursor.execute('SELECT * FROM titanicdata WHERE uuid=%s',id)
		row = cursor.fetchone()
		resp = jsonify(row)
		resp.status_code = 200
		return resp
	except Exception as e:
		print(e)
	finally:
		cursor.close()
		conn.close()

#DELETE /people/{uuid}

@app.route('/people/<id>',methods=['DELETE'])
def delete_people(id):
	try:
		conn = mysql.connect()
		cursor = conn.cursor(pymysql.cursors.DictCursor)
		cursor.execute("DELETE FROM titanicdata WHERE uuid=%s",id)
		conn.commit()
		resp = jsonify('People deleted successfully!')
		resp.status_code = 200
		return resp
	except Exception as e:
		print(e)
	finally:
		cursor.close()
		conn.close()


#PUT /people/{uuid}
@app.route('/people/<uuid>', methods=['PUT'])
def update_people(uuid):
	try:
		_json = request.json
		_survived		 		 = _json['survived']
		_passengerClass  		 = _json['passengerClass']
		_name 			 		 = _json['name']
		_sex 			 		 = _json['sex']
		_age 					 = _json['age']
		_siblingsOrSpousesAboard = _json['siblingsOrSpousesAboard']
		_parentsOrChildrenAboard = _json['parentsOrChildrenAboard']
		_fare 					 = _json['fare']
		# validate the received values
		if _sex and _name and request.method == 'PUT':
			# save edits
			sql = "UPDATE titanicdata SET survived=%s, passengerClass=%s, name=%s, sex=%s, age=%s, siblingsOrSpousesAboard=%s, parentsOrChildrenAboard=%s, fare=%s WHERE uuid=%s"
			data = (_survived, _passengerClass, _name, _sex, _age, _siblingsOrSpousesAboard, _parentsOrChildrenAboard, _fare, uuid,)
			conn = mysql.connect()
			cursor = conn.cursor()
			cursor.execute(sql, data)
			conn.commit()
			resp = jsonify('People updated successfully!')
			resp.status_code = 200
			return resp
		else:
			return not_found()
	except Exception as e:
	 	print(e)
	finally:
	 	cursor.close()
	 	conn.close()

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
