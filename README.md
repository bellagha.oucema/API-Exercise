## 1- Python Application

This is a Python Flask API containing following main parts:

**Structure**

* api/db_init.py         :Database initialization container launched with the start of the container apititanic
* api/main.py            :Flask Python containing the CRUD functions performed on the database
* api/nginx.sh           :Nginx reverse proxy routing traffic to flask application
* api/requirements.txt   :Required Python libraries for the application to run

**Containarization with Docker**

* api/Dockerfile        :Dockerfile containing the commands to assemble the titanicapi image, it's already pushed to Dockerhub and publicly accessible under laghao/apititanic:v1
* docker-compose.yaml   :Running multiple containers and map them together

**Commands to run docker build locally and push to dockerhub**
```bash
cd api
sudo docker build -t laghao/apititanic:v1 .
sudo docker push laghao/apititanic:v1
sudo docker run -p 80:80 laghao/apititanic:v1
```

**Commands to run docker compose**
```bash
sudo docker-compose build
sudo docker-compose up
```
---

## 2- Kubernetes Objects

This is a Kuberentes structure wwhich wraps the Python API containers into deployments, expose them through services and tighten up security with Netwokring Policy and Secret objects.

**Structure**

* api-deployment.yml        :Pod and Service deployment for apititanic
* mariadb-deployment.yml    :Pod, Serviceand secret deployment for mariadb backing up the apititanic
* networkpolicy.yml         :Networking policy securing access to the mariadb database by allowng access to it only from apititanic

**Commands**
```bash
kubectl apply -f k8s/api-deployment.yml
kubectl apply -f k8s/mariadb-deployment.yml
kubectl apply -f k8s/networkPolicy.yml
```
